//
//  main.m
//  BS iOS Test
//
//  Created by BS23 on 1/24/17.
//  Copyright © 2017 Brain Staion 23. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
