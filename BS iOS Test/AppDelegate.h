//
//  AppDelegate.h
//  BS iOS Test
//
//  Created by BS23 on 1/24/17.
//  Copyright © 2017 Brain Staion 23. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

